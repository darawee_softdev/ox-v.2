/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.darawee.xov2;

import java.util.Scanner;

/**
 *
 * @author Darawee
 */
public class Game {

    Player playerX;
    Player playerO;
    Table table;
    Player turn;
    int row, col;
    String answer;
    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void printWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void printTable() {
        table.printTable();
    }

    public void inputPosition() {
        while (true) {
            System.out.println("please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table.setRowCol(row, col)) {
                break;
            }
            System.out.println("Error: table at row and col is not empty!!");

        }

    }

    public void printTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }

    public void run() {
        this.printWelcome();
        while (true) {
            this.printTable();
            this.printTurn();
            this.inputPosition();
            table.checkWin();
            if (checkFinish()) {
                break;
            }
            table.switchPlayer();
        }

    }

    private boolean checkFinish() {
        if (table.isFinish()) {
            this.printTable();
            if (table.getWinner() == null) {
                System.out.println("Draw!!");
            } else {
                System.out.println(table.getWinner().getName() + " Win!!");
            }
            if (checkNewGame()) {
                return true;
            }
        }
        return false;
    }

    private boolean checkNewGame() {
        System.out.println("Do you want to start a new game?(yes, no)");
        answer = kb.next();
        if ("yes".equals(answer)) {
            newGame();
        } else if ("no".equals(answer)) {
            return true;
        }
        return false;
    }

    public void newGame() {
        table = new Table(playerX, playerO);
    }


}
