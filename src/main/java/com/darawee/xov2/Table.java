/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.darawee.xov2;

/**
 *
 * @author Darawee
 */
public class Table {

    public static void inputPosition(int i, int i0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}
    };
     Player playerX;
     Player playerO;
    private Player currentPlayer;
    private Player winner;
    private boolean finish = false;
    private int lastRow;
    private int lastCol;
    private int countTrun = 0;

    

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;
        
    }

    public void printTable() {
        System.out.println("  1 2 3");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + 1 + " ");
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }
    
    public boolean setRowCol(int row, int col) {
       if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastRow = row;
            this.lastCol = col;
            countTrun++;
            return true;
        }
        return false;

    }
    
     public Player getCurrentPlayer() {
        return currentPlayer;
    }
     
     public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
    }
     
    public void checkWin() {
        checkRow();
        checkCol();
        checkX();
        checkDraw();

    }

    void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastCol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    private void setStatWinLose() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerX.win();
            playerO.lose();
        }
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastRow][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    void checkX() {
        if (table[0][2] == currentPlayer.getName()
                && table[1][1] == currentPlayer.getName()
                && table[2][0] == currentPlayer.getName()) {
            finish = true;
            winner = currentPlayer;
            setStatWinLose();
        } else if (table[0][0] == currentPlayer.getName()
                && table[1][1] == currentPlayer.getName()
                && table[2][2] == currentPlayer.getName()) {
            finish = true;
            winner = currentPlayer;
            setStatWinLose();
        }
    }

    void checkDraw() {
        if (countTrun == 9) {
            finish = true;
            playerX.draw();
            playerO.draw();
        }
    }
    
     public boolean isFinish() {
        return finish;
    }

    public Player getWinner() {
        return winner;
    }








}
